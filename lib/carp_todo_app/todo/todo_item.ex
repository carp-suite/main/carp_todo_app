defmodule CarpTodoApp.Todo.TodoItem do
  use Ecto.Schema
  import Ecto.Changeset

  schema "todos" do
    field :checked, :boolean, default: false
    field :description, :string
    field :user_id, :integer

    timestamps()
  end

  @doc false
  def changeset(todo_item, attrs) do
    todo_item
    |> cast(attrs, [:user_id, :description, :checked])
    |> validate_required([:user_id, :description])
  end
end
