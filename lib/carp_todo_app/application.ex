defmodule CarpTodoApp.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      CarpTodoAppWeb.Telemetry,
      # Start the Ecto repository
      CarpTodoApp.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: CarpTodoApp.PubSub},
      # Start Finch
      {Finch, name: CarpTodoApp.Finch},
      # Start the Endpoint (http/https)
      CarpTodoAppWeb.Endpoint
      # Start a worker by calling: CarpTodoApp.Worker.start_link(arg)
      # {CarpTodoApp.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: CarpTodoApp.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    CarpTodoAppWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
