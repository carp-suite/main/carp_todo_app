defmodule CarpTodoApp.User do
  @moduledoc """
  The Todo context.
  """

  import Ecto.Query, warn: false
  alias CarpTodoApp.Todo

  alias CarpTodoApp.User.Owner

  def get_user(user_id) do
    with owner = Owner.changeset(%Owner{id: user_id}, %{}),
         {:valid_owner, true, _} <- {:valid_owner, owner.valid?, owner.errors},
         todos = Todo.list_todos(user_id) do
      {:ok, %{owner.data | todos: todos}}
    else
      {:valid_owner, false, errors} -> {:invalid_owner, errors}
      {:error, error} -> {:error_fetching_todos, error}
    end
  end
end
