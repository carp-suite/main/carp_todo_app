defmodule CarpTodoApp.Repo do
  use Ecto.Repo,
    otp_app: :carp_todo_app,
    adapter: Ecto.Adapters.MyXQL
end
