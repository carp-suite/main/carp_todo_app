defmodule CarpTodoApp.User.Owner do
  use Ecto.Schema
  import Ecto.Changeset
  alias CarpTodoApp.User.Owner

  embedded_schema do
    embeds_many :todos, Todos
  end

  @doc false
  def changeset(%Owner{} = user, attrs) do
    user
    |> cast(attrs, [:id])
    |> validate_required([:id])
    |> validate_number(:id, [gt: 0])
  end
end
