defmodule CarpTodoAppWeb.Router do
  use CarpTodoAppWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {CarpTodoAppWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :authenticated do
    plug CarpTodoAppWeb.Guardian.Pipeline
  end

  if !is_nil(
       dev_user_claims =
         Application.compile_env(:carp_todo_app, :carp_todo_app_dev_user_claims)
     ) do
    pipeline :dev_user do
      plug CarpTodoAppWeb.Guardian.DevUser, claims: dev_user_claims
    end

    scope "/api", CarpTodoAppWeb do
      pipe_through :api
      pipe_through :dev_user
      pipe_through :authenticated

      resources "/todos", TodoItemController, except: [:new, :edit]
    end
  else
    scope "/api", CarpTodoAppWeb do
      pipe_through :api
      pipe_through :authenticated

      resources "/todos", TodoItemController, except: [:new, :edit]
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", CarpTodoAppWeb do
  #   pipe_through :api
  # end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:carp_todo_app, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: CarpTodoAppWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
