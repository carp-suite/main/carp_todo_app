defmodule CarpTodoAppWeb.Guardian do
  use Guardian, otp_app: :carp_todo_app
  alias CarpTodoApp.User.Owner

  def subject_for_token(resource, _claims) when is_struct(resource, Owner) do
    {:ok, resource.id}
  end

  def subject_for_token(_resource, _claims) do
    {:error, :invalid_resource_type}
  end

  def resource_from_claims(%{"sub" => id}) do
    CarpTodoApp.User.get_user(id)
  end

  def resource_from_claims(_) do
    {:error, :cannot_obtain_resources}
  end

  if Application.compile_env!(:carp_todo_app, :disable_jwt_validation) do
    def verify_claims(claims, _options) do
      {:ok, claims}
    end
  end
end
