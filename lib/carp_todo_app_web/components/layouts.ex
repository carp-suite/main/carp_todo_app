defmodule CarpTodoAppWeb.Layouts do
  use CarpTodoAppWeb, :html

  embed_templates "layouts/*"
end
