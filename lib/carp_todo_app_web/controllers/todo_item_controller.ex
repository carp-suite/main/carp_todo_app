defmodule CarpTodoAppWeb.TodoItemController do
  use CarpTodoAppWeb, :controller

  alias CarpTodoApp.Todo
  alias CarpTodoApp.Todo.TodoItem
  alias CarpTodoAppWeb.Guardian.Plug, as: GuardianPlug

  action_fallback CarpTodoAppWeb.FallbackController

  def index(conn, _params) do
    owner = GuardianPlug.current_resource(conn)
    render(conn, :index, todo_items: owner.todos)
  end

  def create(conn, %{"todo_item" => todo_item_input_params}) do
    owner_claims = GuardianPlug.current_claims(conn)

    todo_item_params =
      todo_item_input_params |> Map.put("user_id", owner_claims["sub"])

    with {:ok, %TodoItem{} = todo_item} <- Todo.create_todo_item(todo_item_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/todos/#{todo_item}")
      |> render(:show, todo_item: todo_item)
    end
  end

  def show(conn, %{"id" => id}) do
    todo_item = Todo.get_todo_item!(id)
    render(conn, :show, todo_item: todo_item)
  end

  def update(conn, %{"id" => id, "todo_item" => todo_item_params}) do
    todo_item = Todo.get_todo_item!(id)

    with {:ok, %TodoItem{} = todo_item} <- Todo.update_todo_item(todo_item, todo_item_params) do
      render(conn, :show, todo_item: todo_item)
    end
  end

  def delete(conn, %{"id" => id}) do
    todo_item = Todo.get_todo_item!(id)

    with {:ok, %TodoItem{}} <- Todo.delete_todo_item(todo_item) do
      send_resp(conn, :no_content, "")
    end
  end
end
