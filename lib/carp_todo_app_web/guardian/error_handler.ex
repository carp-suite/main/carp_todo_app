defmodule CarpTodoAppWeb.Guardian.ErrorHandler do
  require Logger
  @behaviour Guardian.Plug.ErrorHandler

  @impl Guardian.Plug.ErrorHandler
  def auth_error(conn, {type, message}, _params) do
    Logger.warning("Got error #{type}: #{message} while trying to authenticate")

    conn
    |> Phoenix.Controller.redirect(external: "http://localhost/some-other/service")
  end
end
