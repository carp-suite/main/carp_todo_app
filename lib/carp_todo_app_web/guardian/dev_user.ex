defmodule CarpTodoAppWeb.Guardian.DevUser do
  import Plug.Conn
  require Logger
  alias CarpTodoAppWeb.Guardian
  alias CarpTodoApp.User.Owner

  def init(opts), do: Keyword.fetch!(opts, :claims)

  def call(conn, claims) do
    claims_map = Enum.into(claims, %{})
    {:ok, token, _} = Guardian.encode_and_sign(%Owner{id: claims_map.sub}, claims_map)
    Logger.debug("Attaching token to request: #{token} with claims #{inspect(claims_map)}")

    conn
    |> put_req_header("authorization", "Bearer #{token}")
  end
end
