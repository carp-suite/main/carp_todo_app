defmodule CarpTodoAppWeb.Guardian.Pipeline do
  use Guardian.Plug.Pipeline, otp_app: :carp_todo_app

  plug Guardian.Plug.VerifyHeader
  plug Guardian.Plug.EnsureAuthenticated
  plug Guardian.Plug.LoadResource
end
