defmodule CarpTodoApp.Repo.Migrations.CreateTodos do
  use Ecto.Migration

  def change do
    create table(:todos) do
      add :user_id, :integer
      add :description, :string
      add :checked, :boolean, default: false, null: false

      timestamps()
    end
  end
end
