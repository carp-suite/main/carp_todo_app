import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :carp_todo_app, CarpTodoApp.Repo,
  username: "carp_todo_app",
  password: "KHearts3",
  hostname: "localhost",
  database: "carp_todo_db_test",
  stacktrace: true,
  show_sensitive_data_on_connection_error: true,
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

config :carp_todo_app, :disable_jwt_validation, false

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :carp_todo_app, CarpTodoAppWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "DDqvET17T3yEbBVa0GSiK1oWx59h4Mkm7qiuZpOl40euhp5/e7sqFEENfq7agfei",
  server: false

# In test we don't send emails.
config :carp_todo_app, CarpTodoApp.Mailer, adapter: Swoosh.Adapters.Test

# Disable swoosh api client as it is only required for production adapters.
config :swoosh, :api_client, false

# Print only warnings and errors during test
config :logger, level: :warning

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
