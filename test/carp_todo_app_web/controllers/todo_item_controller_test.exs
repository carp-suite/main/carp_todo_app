defmodule CarpTodoAppWeb.TodoItemControllerTest do
  use CarpTodoAppWeb.ConnCase

  import CarpTodoApp.TodoFixtures

  @create_attrs %{checked: true, description: "some description", user_id: 42}
  @update_attrs %{checked: false, description: "some updated description", user_id: 43}
  @invalid_attrs %{checked: nil, description: nil, user_id: nil}
  @dummy_token "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2OTIzOTMxOTEsImlzcyI6ImNhcnAudG9kb19hcHAuYXV0aCIsImp0aSI6IjcyN2U5OWIyLWExYjItNDA0NC1iNDZhLTE2NmE0ZTkxYmFlNCIsIm5iZiI6MTY5MjM5MzE5MCwic3ViIjoxLCJ0eXAiOiJhY2Nlc3MifQ.7yAq_5t89VBXqUnfeGCNoCRke1Eah5u2Xd46vJ6wx6sEIJXDItm0C88zPspH43l66s-RryO5WUraIpSBgObwOA"

  describe "index" do
    setup [:create_todo_item]

    test "should return redirect if request is not authorized", %{conn: conn} do
      conn = get(conn, ~p"/api/todos")
      assert redirected_to(conn) == "http://localhost/some-other/service"
    end

    test "lists all todos of user", %{conn: conn} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{@dummy_token}")
        |> get(~p"/api/todos")

      assert List.first(json_response(conn, 200)["data"])["description"] === "some description"
    end
  end

  describe "create todo_item" do
    test "should return redirect if request is not authorized", %{conn: conn} do
      conn = post(conn, ~p"/api/todos", todo_item: @create_attrs)
      assert redirected_to(conn) == "http://localhost/some-other/service"
    end

    test "shows data if create is successful", %{conn: conn} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{@dummy_token}")
        |> post(~p"/api/todos", todo_item: @create_attrs)

      assert json_response(conn, 201)["data"]["description"] === "some description"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{@dummy_token}")
        |> post(~p"/api/todos", todo_item: @invalid_attrs)

      assert json_response(conn, 400)
    end
  end

  describe "update todo_item" do
    setup [:create_todo_item]

    test "should return redirect if request is not authorized", %{
      conn: conn,
      todo_item: todo_item
    } do
      conn = put(conn, ~p"/api/todos/#{todo_item}")
      assert redirected_to(conn) == "http://localhost/some-other/service"
    end

    test "renders data when valid", %{conn: conn, todo_item: todo_item} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{@dummy_token}")
        |> put(~p"/api/todos/#{todo_item}", todo_item: @update_attrs)

      assert json_response(conn, 200)["data"]["description"] =~ "some updated description"
    end

    test "renders errors when data is invalid", %{conn: conn, todo_item: todo_item} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{@dummy_token}")
        |> put(~p"/api/todos/#{todo_item}", todo_item: @invalid_attrs)

      assert json_response(conn, 400)
    end
  end

  describe "delete todo_item" do
    setup [:create_todo_item]

    test "should return redirect if request is not authorized", %{
      conn: conn,
      todo_item: todo_item
    } do
      conn = delete(conn, ~p"/api/todos/#{todo_item}")
      assert redirected_to(conn) == "http://localhost/some-other/service"
    end

    test "deletes chosen todo_item", %{conn: conn, todo_item: todo_item} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{@dummy_token}")
        |> delete(~p"/api/todos/#{todo_item}")

      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, ~p"/api/todos/#{todo_item}")
      end
    end
  end

  defp create_todo_item(_) do
    todo_item = todo_item_fixture()
    %{todo_item: todo_item}
  end
end
