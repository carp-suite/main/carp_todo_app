defmodule CarpTodoApp.TodoFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `CarpTodoApp.Todo` context.
  """

  @doc """
  Generate a todo_item.
  """
  def todo_item_fixture(attrs \\ %{}) do
    {:ok, todo_item} =
      attrs
      |> Enum.into(%{
        checked: true,
        description: "some description",
        user_id: 1
      })
      |> CarpTodoApp.Todo.create_todo_item()

    todo_item
  end
end
