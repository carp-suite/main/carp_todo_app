defmodule CarpTodoApp.TodoTest do
  use CarpTodoApp.DataCase
  alias CarpTodoAppWeb.Guardian

  alias CarpTodoApp.Todo

  describe "todos" do
    alias CarpTodoApp.Todo.TodoItem

    import CarpTodoApp.TodoFixtures

    @invalid_attrs %{checked: nil, description: nil, user_id: nil}

    test "list_todos/0 returns all todos" do
      todo_item = todo_item_fixture()
      assert Todo.list_todos() == [todo_item]
    end

    test "get_todo_item!/1 returns the todo_item with given id" do
      todo_item = todo_item_fixture()
      assert Todo.get_todo_item!(todo_item.id) == todo_item
    end

    test "create_todo_item/1 with valid data creates a todo_item" do
      valid_attrs = %{checked: true, description: "some description", user_id: 42}

      assert {:ok, %TodoItem{} = todo_item} = Todo.create_todo_item(valid_attrs)
      assert todo_item.checked == true
      assert todo_item.description == "some description"
      assert todo_item.user_id == 42
    end

    test "create_todo_item/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Todo.create_todo_item(@invalid_attrs)
    end

    test "update_todo_item/2 with valid data updates the todo_item" do
      todo_item = todo_item_fixture()
      update_attrs = %{checked: false, description: "some updated description", user_id: 43}

      assert {:ok, %TodoItem{} = todo_item} = Todo.update_todo_item(todo_item, update_attrs)
      assert todo_item.checked == false
      assert todo_item.description == "some updated description"
      assert todo_item.user_id == 43
    end

    test "update_todo_item/2 with invalid data returns error changeset" do
      todo_item = todo_item_fixture()
      assert {:error, %Ecto.Changeset{}} = Todo.update_todo_item(todo_item, @invalid_attrs)
      assert todo_item == Todo.get_todo_item!(todo_item.id)
    end

    test "delete_todo_item/1 deletes the todo_item" do
      todo_item = todo_item_fixture()
      assert {:ok, %TodoItem{}} = Todo.delete_todo_item(todo_item)
      assert_raise Ecto.NoResultsError, fn -> Todo.get_todo_item!(todo_item.id) end
    end

    test "change_todo_item/1 returns a todo_item changeset" do
      todo_item = todo_item_fixture()
      assert %Ecto.Changeset{} = Todo.change_todo_item(todo_item)
    end
  end
end
